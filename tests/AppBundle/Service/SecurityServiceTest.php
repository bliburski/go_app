<?php

namespace Tests\AppBundle\Service;

use AppBundle\Exception\ApiValidationException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Service\SecurityService;

class SecurityServiceTest extends WebTestCase
{
    private $securityService;
    public function setUp()
    {
        $kernel = self::bootKernel();
        $this->securityService = $kernel->getContainer()->get('app.testing');
    }

    public function testRegisterUserWithNewCompany()
    {
        $email = substr(md5(uniqid(null, true)), 0, 12);
        $json = array('firstName'=> 'Bart', 'lastName'=> 'name', 'email'=> $email.'@test.com',
            'password'=> '123qwe123', 'companyName'=> 'test');

        $result = $this->securityService->registerUser(json_encode($json), true);

        $this->assertEquals(true, $result);
    }

    public function testRegisterUserToExistingCompany()
    {
        $email = substr(md5(uniqid(null, true)), 0, 12);
        $json = array('firstName'=> 'Bart', 'lastName'=> 'name', 'email'=> $email.'@test.com',
            'companyDetailsId'=> 1);

        $result = $this->securityService->registerUser(json_encode($json), false);

        $this->assertEquals(true, $result);
    }


    public function testRegisterUserInvalidForm()
    {
        $this->expectException(ApiValidationException::class);
        $json = array('firstName'=> 'Bart', 'lastName'=> 'name', 'email'=> 'brak.com',
            'password'=> '123qwe123', 'companyName'=> 'test');

        $this->securityService->registerUser(json_encode($json), false);
    }
}