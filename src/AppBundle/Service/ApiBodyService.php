<?php

namespace AppBundle\Service;

use AppBundle\Exception\ApiValidationException;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class ApiBodyService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ApiBodyService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $em
     */
    public function __construct(ValidatorInterface $validator)
    {
        $encoders = array(new JsonEncoder());
        $objectNormalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $objectNormalizer->setCircularReferenceLimit(1);
        $objectNormalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($objectNormalizer, new PropertyNormalizer(), new ArrayDenormalizer(),
            new GetSetMethodNormalizer());

        $this->serializer = new Serializer($normalizers, $encoders);
        $this->validator = $validator;
    }

    /**
     * @param  $object
     * @return string
     */
    public function serialize(object $object): string
    {
        return $this->serializer->serialize($object, 'json');
    }

    /**
     * @param string $json
     * @param string $type
     * @return mixed
     */
    public function deserialize(string $json, string $type): object
    {
        return $this->serializer->deserialize($json, $type, 'json');
    }

    /**
     * @param array $data
     * @param string $type
     * @return object
     */
    public function denormalize(array $data, string $type): object
    {
        return $this->serializer->denormalize($data, $type);
    }

    /**
     * @param $json
     * @param $type
     * @return array
     */
    public function arrayDeserialize(string $json, mixed $type): array
    {
        return $this->serializer->deserialize($json, $type . '[]', 'json');
    }

    /**
     * @param array $data
     * @param string $type = entityClass
     * @return array
     */
    public function arrayDenormalize(array $data, string $type): array
    {
        return $this->serializer->denormalize($data, $type . '[]');
    }

    /**
     * @param $object
     * @throws ApiValidationException
     */
    public function validateOrThrow($object): void
    {
        $errors = $this->validator->validate($object);

        if (count($errors) > 0) {
            throw new ApiValidationException($errors);
        }
    }
}
