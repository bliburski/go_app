<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpFoundation\Response;

class BaseService
{
    protected $doctrine;
    protected $encoderFactory;
    protected $apiBodyService;
    protected $dateTime;
    protected $translator;

    /**
     * BaseService constructor.
     * @param Doctrine $entityManager
     * @param EncoderFactory $encoderFactory
     * @param ApiBodyService $apiBodyService
     * @param Translator $translator
     */
    public function __construct(Doctrine $entityManager, EncoderFactory $encoderFactory,
                                ApiBodyService $apiBodyService, Translator $translator)
    {
        $this->dateTime = new \DateTime('now');
        $this->doctrine = $entityManager;
        $this->encoderFactory = $encoderFactory;
        $this->apiBodyService = $apiBodyService;
        $this->translator = $translator;
    }

    /**
     * @param $key
     * @return JsonResponse
     */
    protected function returnError(string $key): JsonResponse
    {
        if ($key == null) {
            $key = 'general';
        }
        return new JsonResponse(['error' => $this->translator->trans('JsonError' . $key)],
            Response::HTTP_PAYMENT_REQUIRED);
    }

    /**
     * @param $id
     * @return object
     */
    protected function getCompanyDetailsObject(int $id): object
    {
        $em = $this->doctrine->getManager();
        $companyDetails = $em->find('AppBundle\Entity\CompanyDetails', $id);
        $companyDetails === null ? $this->returnError('.noCompanyDetailsWithId') : true;
        return $companyDetails;
    }

    /**
     * @param $id
     * @return object
     */
    protected function getUserObject(int $id): object
    {
        $em = $this->doctrine->getManager();
        $companyDetails = $em->find('AppBundle\Entity\User', $id);
        $companyDetails === null ? $this->returnError('.noUserWithId') : true;
        return $companyDetails;
    }

    /**
     * @param $object
     * @param $criteria
     * @return object
     */
    protected function getEntityObject(string $object, mixed $criteria): object
    {
        if (is_array($criteria)) {
            $em = $this->doctrine->getRepository('AppBundle\Entity\\' . $object);
            $object = $em->findOneBy($criteria);
        } else {
            $em = $this->doctrine->getManager();
            $object = $em->find('AppBundle\Entity\\' . $object, $criteria);
        }
        $object === null ? $this->returnError('.noDictionaryDefinition') : true;
        return $object;
    }

    /**
     * @param $uniqueElement
     * @param $length
     * @return bool|string
     */
    protected function generateUniqueToken(string $uniqueElement, int $length): string
    {
        return substr(md5(uniqid($uniqueElement, true)), 0, $length);
    }
}

