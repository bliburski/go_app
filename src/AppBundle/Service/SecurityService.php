<?php

namespace AppBundle\Service;

use AppBundle\Entity\CompanyDetails;
use AppBundle\Entity\User;
use AppBundle\ApiBody\RegisterUserApiBody;
use AppBundle\ApiBody\CompanyConfigurationStepTwoBody;

class SecurityService extends BaseService
{
    /**
     * @param string $body
     * @param bool $isNewUser
     * @return bool
     */
    public function registerUser(string $body, bool $isNewUser): bool
    {
        $em = $this->doctrine->getManager();
        $userEntity = new User();
        $encoder = $this->encoderFactory->getEncoder($userEntity);
        if ($isNewUser === false) {//create user to existing company
            $data = $this->apiBodyService->deserialize($body, CompanyConfigurationStepTwoBody::class);
            $this->apiBodyService->validateOrThrow($data);
            $companyDetailsEntity = $em->getRepository(CompanyDetails::class)->find($data->companyDetailsId);
            $plainPassword = $this->getRandomPassword();
            $encodedPassword = $encoder->encodePassword($plainPassword, $userEntity->getSalt());
            $userEntity->setPassword($encodedPassword);
            $userEntity->setCompanyDetails($companyDetailsEntity);
            //TODO send mail with plainPassword
        } else {// register new company and new user
            $data = $this->apiBodyService->deserialize($body, RegisterUserApiBody::class);
            $this->apiBodyService->validateOrThrow($data);
            $companyDetailsEntity = new CompanyDetails();
            $companyDetailsEntity->setCompanyName($data->companyName);
            $userEntity->setCompanyDetails($companyDetailsEntity);
            $password = $encoder->encodePassword($data->password, $userEntity->getSalt());
            $userEntity->setPassword($password);
            $em->persist($companyDetailsEntity);
        }
        $userEntity->setFirstname($data->firstName);
        $userEntity->setLastname($data->lastName);
        $userEntity->setEmail($data->email);
        $userEntity->setRoles(array('ROLE_CLIENT'));
        $userEntity->setRegisterTime($this->dateTime);

        $em->persist($userEntity);
        $em->flush();
        return true;
    }

    /**
     * @param int $length
     * @return string
     */
    private function getRandomPassword(int $length = 8): string
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}