<?php

namespace AppBundle\ApiBody;

use Symfony\Component\Validator\Constraints as Assert;

class CompanyConfigurationStepTwoBody
{
    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 128
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $firstName;

    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 128
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $lastName;

    /**
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    public $email;

    /**
     * @Assert\Type("int")
     */
    public $companyDetailsId;
}