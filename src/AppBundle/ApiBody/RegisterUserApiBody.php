<?php

namespace AppBundle\ApiBody;

use Symfony\Component\Validator\Constraints as Assert;

class RegisterUserApiBody
{
    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 128
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $firstName;

    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 128
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $lastName;

    /**
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    public $email;

    /**
     * @Assert\Length(
     *      min = 6,
     *      max = 32
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $password;

    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 256
     * )
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $companyName;
}