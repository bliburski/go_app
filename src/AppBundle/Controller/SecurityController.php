<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\SecurityService;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
    private $model;

    /**
     * SecurityController constructor.
     * @param SecurityService $securityService
     */
    public function __construct(SecurityService $securityService)
    {
        $this->model = $securityService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function registerAction(Request $request): JsonResponse
    {
        $body = $request->getContent();
        if ($body != null) {
            $this->model->registerUser($body, true);
            return new JsonResponse(true);
        }
        return new JsonResponse(['message' => $this->get('translator')->trans('err.noData')],
            Response::HTTP_PAYMENT_REQUIRED);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function registerWithCompanyAction(Request $request): JsonResponse
    {
        $body = $request->getContent();
        if ($body != null) {
            $this->model->registerUser($body, false);
            return new JsonResponse(true);
        }
        return new JsonResponse(['message' => $this->get('translator')->trans('err.noData')],
            Response::HTTP_PAYMENT_REQUIRED);
    }

    /**
     * @return JsonResponse
     */
    public function loginAction(): JsonResponse
    {
        return new JsonResponse();

    }
}
